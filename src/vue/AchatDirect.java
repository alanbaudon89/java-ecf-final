package vue;

/**
* Declarations des imports
*/

import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DateFormatter;

import module.HistoAchats;
import module.Medicament;

import javax.swing.border.BevelBorder;
import javax.swing.border.MatteBorder;
import java.awt.Color;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.net.http.WebSocket.Listener;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.ActionEvent;
import java.awt.Rectangle;
import javax.swing.JComboBox;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

public class  AchatDirect extends JFrame implements ItemListener{
	
	
	private JPanel contentPane;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JButton btnNewButton;
	private JButton btnOrdonance;
	private JPanel panel;
	private JTextField textField_2;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_2_1;
	private JLabel lblNewLabel_2_2;
	private JLabel lblNewLabel_2_3;
	private JLabel lblNewLabel_2_4;
	private JLabel lblNewLabel_2_5;
	private JLabel prixUnitaire;
	private JTextField NomClient;
	private JLabel lblNewLabel_2_5_1;
	private JButton btnNewButton_1;
	private JLabel lblNewLabel_2_5_2;
	private JTextField PrenomClient;
	private JButton btnNewButton_1_1;
	private JButton btnNewButton_1_1_1;
	private JLabel prixTotal;
	private JButton btnValider;
	private JButton btnPay;
	private JLabel lblNewLabel_4;
	public Double pxAuto;
	public JLabel lblNewLabel_3;
	ArrayList<HistoAchats> Achat = new ArrayList<HistoAchats>();
	JComboBox<Object> comboBox1 = new JComboBox<Object>();
	Medicament medicPara;
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
	
	
	/**
	 * @Launch the application.
	 */
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AchatDirect frame = new AchatDirect();
					frame.setVisible(true);
					frame.setLocation(200, 200);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}

	/**
	 * @Create the frame.
	 */
	
	public AchatDirect() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 632);
		contentPane = new JPanel();
		contentPane.setBounds(new Rectangle(0, 0, 50, 50));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(61)
							.addComponent(getBtnNewButton_1()))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(119)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(getBtnNewButton())
									.addGap(108)
									.addComponent(getLblNewLabel_1())
									.addGap(85)
									.addComponent(getBtnOrdonance()))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(getLblNewLabel_2_5_2(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(getPrenomClient(), GroupLayout.PREFERRED_SIZE, 176, GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(141)
									.addComponent(getLblNewLabel()))
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(getLblNewLabel_2_5(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(getNomClient(), GroupLayout.PREFERRED_SIZE, 176, GroupLayout.PREFERRED_SIZE)))))
					.addContainerGap(130, Short.MAX_VALUE))
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addGap(184)
					.addComponent(getLblNewLabel_2_4())
					.addGap(87)
					.addComponent(getPrixTotal(), GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(getBtnPay())
					.addContainerGap(75, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 587, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(20)
							.addComponent(getBtnNewButton_1_1(), GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 463, Short.MAX_VALUE)
							.addComponent(getBtnNewButton_1_1_1(), GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)))
					.addGap(51))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(32)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
							.addComponent(getBtnOrdonance())
							.addComponent(getBtnNewButton())
							.addGroup(gl_contentPane.createSequentialGroup()
								.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
									.addComponent(getBtnNewButton_1_1_1(), GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
									.addComponent(getLblNewLabel()))
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(getLblNewLabel_1())
								.addGap(16)))
						.addComponent(getBtnNewButton_1_1(), GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblNewLabel_2_5(), GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
						.addComponent(getNomClient(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblNewLabel_2_5_2(), GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
						.addComponent(getPrenomClient(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getBtnNewButton_1())
							.addGap(6)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(getLblNewLabel_2_4())
								.addComponent(getPrixTotal(), GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(35)
							.addComponent(getBtnPay())))
					.addContainerGap())
		);
		contentPane.setLayout(gl_contentPane);
	}

	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("Sparadrap");
			lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 26));
		}
		return lblNewLabel;
	}
	private JLabel getLblNewLabel_1() {
		if (lblNewLabel_1 == null) {
			lblNewLabel_1 = new JLabel("");
			lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\User-02\\Documents\\Projets\\Sparadrap\\src\\images\\My project-1.png"));
		}
		return lblNewLabel_1;
	}
	private JButton getBtnNewButton() {
		if (btnNewButton == null) {
			btnNewButton = new JButton("Direct");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new AchatDirect();
					AchatDirect achatDirect = new AchatDirect();
					achatDirect.setVisible(true);
					achatDirect.setTitle("Achat Direct");
					dispose();
				}
			});
		}
		return btnNewButton;
	}
	private JButton getBtnOrdonance() {
		if (btnOrdonance == null) {
			btnOrdonance = new JButton("Ordonnance");
			btnOrdonance.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new AchatOrdonnance();
					AchatOrdonnance achatOrdonnance = new AchatOrdonnance();
					achatOrdonnance.setVisible(true);
					achatOrdonnance.setTitle("Achat Ordonnance");
					dispose();
				}
			});
		}
		return btnOrdonance;
	}
	public JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
			panel.setLayout(null);
			panel.add(getTextField_2());
			panel.add(getLblNewLabel_2());
			panel.add(getLblNewLabel_2_1());
			panel.add(getLblNewLabel_2_2());
			panel.add(getLblNewLabel_2_3());
			panel.add(getLblNewLabel_2_5_1());
			panel.add(getBtnValider());
			
			
			Medicament medicPara = new Medicament("Paracétamol", "Antalgique 1", 2.18,"17-12-2019",500 );
			Medicament medicDafa = new Medicament("Dafalgan", "Antalgique 2", 4.50,"18-12-2019",650 );
			Medicament medicIbu = new Medicament("Ibuprofène", "Antalgique 1", 3.20,"19-12-2019",750 );
			ArrayList<Medicament> medoc = new ArrayList<Medicament>();
			medoc.add(medicIbu);
			medoc.add(medicPara);
			medoc.add(medicDafa);
			comboBox1.setMaximumRowCount(20);
			comboBox1.setBounds(276, 71, 174, 22);
			for(Medicament medicament : medoc)
			{
				comboBox1.addItem(medicament);
			}
			panel.add(comboBox1);
			prixUnitaire = new JLabel(""+((Medicament) comboBox1.getSelectedItem()).getPrix());
			comboBox1.addItemListener(this);
			prixUnitaire.setBackground(Color.WHITE);
			prixUnitaire.setBounds(274, 114, 176, 20);
			panel.add(prixUnitaire);
			panel.add(getLblNewLabel_4());

			JLabel lblNewLabel_3 = new JLabel(LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
			lblNewLabel_3.setBounds(272, 29, 95, 16);
			panel.add(lblNewLabel_3);


		}
		return panel;
		
		
	}
	public  JTextField getTextField_2() {
			textField_2 = new JTextField();
			textField_2.setBounds(272, 160, 85, 20);
			
			return textField_2;
	}

		
		
		
	
	private JLabel getLblNewLabel_2() {
		if (lblNewLabel_2 == null) {
			lblNewLabel_2 = new JLabel("Médicament");
			lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNewLabel_2.setBounds(125, 73, 95, 14);
		}
		return lblNewLabel_2;
	}
	private JLabel getLblNewLabel_2_1() {
		if (lblNewLabel_2_1 == null) {
			lblNewLabel_2_1 = new JLabel("Prix unitaire");
			lblNewLabel_2_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNewLabel_2_1.setBounds(125, 118, 85, 14);
		}
		return lblNewLabel_2_1;
	}
	private JLabel getLblNewLabel_2_2() {
		if (lblNewLabel_2_2 == null) {
			lblNewLabel_2_2 = new JLabel("Quantité");
			lblNewLabel_2_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNewLabel_2_2.setBounds(125, 161, 67, 14);
		}
		return lblNewLabel_2_2;
	}
	private JLabel getLblNewLabel_2_3() {
		if (lblNewLabel_2_3 == null) {
			lblNewLabel_2_3 = new JLabel("Stock");
			lblNewLabel_2_3.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNewLabel_2_3.setBounds(125, 199, 46, 14);
		}
		return lblNewLabel_2_3;
	}
	private JLabel getLblNewLabel_2_4() {
		if (lblNewLabel_2_4 == null) {
			lblNewLabel_2_4 = new JLabel("Prix Total");
			lblNewLabel_2_4.setFont(new Font("Tahoma", Font.PLAIN, 16));
		}
		return lblNewLabel_2_4;
	}
	private JLabel getLblNewLabel_2_5() {
		if (lblNewLabel_2_5 == null) {
			lblNewLabel_2_5 = new JLabel("Nom");
			lblNewLabel_2_5.setFont(new Font("Tahoma", Font.PLAIN, 16));
		}
		return lblNewLabel_2_5;
	}
	private JTextField getNomClient() {
		if (NomClient == null) {
			NomClient = new JTextField();
			NomClient.setColumns(10);
		}
		return NomClient;
	}
	private JLabel getLblNewLabel_2_5_1() {
		if (lblNewLabel_2_5_1 == null) {
			lblNewLabel_2_5_1 = new JLabel("Date");
			lblNewLabel_2_5_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNewLabel_2_5_1.setBounds(125, 29, 95, 14);
		}
		return lblNewLabel_2_5_1;
	}
	private JButton getBtnNewButton_1() {
		if (btnNewButton_1 == null) {
			btnNewButton_1 = new JButton("Ajouter");
			btnNewButton_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				}
			});
		}
		return btnNewButton_1;
	}
	private JLabel getLblNewLabel_2_5_2() {
		if (lblNewLabel_2_5_2 == null) {
			lblNewLabel_2_5_2 = new JLabel("Prénom");
			lblNewLabel_2_5_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		}
		return lblNewLabel_2_5_2;
	}
	private JTextField getPrenomClient() {
		if (PrenomClient == null) {
			PrenomClient = new JTextField();
			PrenomClient.setColumns(10);
		}
		return PrenomClient;
	}
	private JButton getBtnNewButton_1_1() {
		if (btnNewButton_1_1 == null) {
			btnNewButton_1_1 = new JButton("");
			btnNewButton_1_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new Accueil();
					Accueil accueil = new Accueil();
					accueil.setVisible(true);
					dispose();
				}
			});
			btnNewButton_1_1.setSelectedIcon(null);
			btnNewButton_1_1.setIcon(new ImageIcon("C:\\Users\\User-02\\Documents\\Projets\\Sparadrap\\src\\images\\en-arriere.png"));
			btnNewButton_1_1.setBounds(new Rectangle(0, 0, 50, 50));
		}
		return btnNewButton_1_1;
	}
	private JButton getBtnNewButton_1_1_1() {
		if (btnNewButton_1_1_1 == null) {
			btnNewButton_1_1_1 = new JButton("");
			btnNewButton_1_1_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			btnNewButton_1_1_1.setIcon(new ImageIcon("C:\\Users\\User-02\\Documents\\Projets\\Sparadrap\\src\\images\\bouton-quitter-vers-lapplication.png"));
			btnNewButton_1_1_1.setBounds(new Rectangle(0, 0, 50, 50));
		}
		return btnNewButton_1_1_1;
	}
	private JLabel getPrixTotal() {
		if (prixTotal == null) {
			prixTotal = new JLabel ("0 " + " €");
		}
		return prixTotal;
	}
	private JButton getBtnValider() {
		if (btnValider == null) {
			btnValider = new JButton("Valider");
			btnValider.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
				double pxToC = ((Medicament) comboBox1.getSelectedItem()).getPrix();
				double qteToC = Integer.parseInt(textField_2.getText());
				
				double pxTotal = pxToC * qteToC;
				DecimalFormat dFG = new DecimalFormat("0.00");
				String str = dFG.format(pxTotal);
				str = str.replaceAll(",", "."); 
				prixTotal.setText(String.valueOf(dFG.format(pxTotal)));
				
				
			}
			});
			btnValider.setBounds(469, 159, 79, 23);
		}
		return btnValider;
	}
	private JButton getBtnPay() {
		if (btnPay == null) {
			btnPay = new JButton("Enregistrer");
			btnPay.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new ArrayList<HistoAchats>();
					LocalDate date = LocalDate.parse("2022-09-01");
					
					new HistoAchats(date,((Medicament) comboBox1.getSelectedItem()).getNomMedicament(),
							Double.valueOf(prixTotal.getText()),
							NomClient.getText(),
							PrenomClient.getText(),
							((Medicament) comboBox1.getSelectedItem()).getQuantite(),
							true);
					
					/*ArrayList<HistoAchats> Achat = new ArrayList<HislblNewLabel_3.toAchats>();
					HistoAchats achat1 = new HistoAchats( "Paracétamol", 2.18,"Ebuy1","Font1",5,true);
					HistoAchats achat2 = new HistoAchats( "Paracétamol", 4.50,"Ebuy2","Font2",5,true );
					HistoAchats achat3 = new HistoAchats( "Ibuprofène", 3.20,"Ebuy3","Font3",5,true);*/
					
				}
			});
		}
		return btnPay;
	}
	private JLabel getLblNewLabel_4() {
		if (lblNewLabel_4 == null) {
			lblNewLabel_4 = new JLabel(""+((Medicament) comboBox1.getSelectedItem()).getQuantite());
			lblNewLabel_4.setBounds(274, 201, 176, 14);
		}
		return lblNewLabel_4;
	}

	public void itemStateChanged(ItemEvent e) { 
	 if(e.getSource()==comboBox1)
	 {
		 prixUnitaire.setText(""+((Medicament) comboBox1.getSelectedItem()).getPrix()+" €");
		 lblNewLabel_4.setText(""+((Medicament) comboBox1.getSelectedItem()).getQuantite());
		 
		 }
	 }
	}
