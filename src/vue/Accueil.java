package vue;

/**
* Declarations des imports
*/ 
import controleur.*;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import java.awt.TextField;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.Rectangle;

/*
 * @Jframe
 * 
 */
public class Accueil extends JFrame {

	private JPanel contentPane;
	private JButton btnNewButton;
	private JButton btnHistoriqueDesAchats;
	private JButton btnHistoriqueDesOrdonnances;
	private JButton btnDtailsClients;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JButton btnNewButton_1_1_1;

	/**
	 * @Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Accueil frame = new Accueil();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * @Create the frame.
	 */
	public Accueil() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(252)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(getLblNewLabel(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
								.addComponent(getBtnNewButton(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
								.addComponent(getBtnHistoriqueDesAchats(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
								.addComponent(getBtnHistoriqueDesOrdonnances(), Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(getBtnDtailsClients(), GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(295)
							.addComponent(getLblNewLabel_1())))
					.addGap(145)
					.addComponent(getBtnNewButton_1_1_1(), GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addGap(40))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(25)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(getBtnNewButton_1_1_1(), GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(getLblNewLabel())
							.addGap(36)
							.addComponent(getLblNewLabel_1())
							.addGap(26)
							.addComponent(getBtnNewButton())
							.addGap(18)
							.addComponent(getBtnHistoriqueDesAchats())
							.addGap(18)
							.addComponent(getBtnHistoriqueDesOrdonnances())
							.addGap(18)
							.addComponent(getBtnDtailsClients())))
					.addContainerGap(97, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}
	private JButton getBtnNewButton() {
		if (btnNewButton == null) {
			btnNewButton = new JButton("Effectuer un Achat");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new AchatDirect();
					AchatDirect achatDirect = new AchatDirect();
					achatDirect.setVisible(true);
					achatDirect.setTitle("Achat Direct");
					dispose();
				}
			});
		}
		return btnNewButton;
	}
	private JButton getBtnHistoriqueDesAchats() {
		if (btnHistoriqueDesAchats == null) {
			btnHistoriqueDesAchats = new JButton("Historique des Achats");
			btnHistoriqueDesAchats.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new HistoriqueAchats();
					HistoriqueAchats historiqueAchats = new HistoriqueAchats();
					historiqueAchats.setVisible(true);
					historiqueAchats.setTitle("Historique Achats");
					dispose();
				}
			});
		}
		return btnHistoriqueDesAchats;
	}
	private JButton getBtnHistoriqueDesOrdonnances() {
		if (btnHistoriqueDesOrdonnances == null) {
			btnHistoriqueDesOrdonnances = new JButton("Historique des Ordonnances");
		}
		return btnHistoriqueDesOrdonnances;
	}
	private JButton getBtnDtailsClients() {
		if (btnDtailsClients == null) {
			btnDtailsClients = new JButton("Détails Clients");
		}
		return btnDtailsClients;
	}
	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("  Sparadrap");
			lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 26));
		}
		return lblNewLabel;
	}
	private JLabel getLblNewLabel_1() {
		if (lblNewLabel_1 == null) {
			lblNewLabel_1 = new JLabel("");
			lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\User-02\\Documents\\Projets\\Sparadrap\\src\\images\\My project-1.png"));
		}
		return lblNewLabel_1;
	}
	private JButton getBtnNewButton_1_1_1() {
		if (btnNewButton_1_1_1 == null) {
			btnNewButton_1_1_1 = new JButton("");
			btnNewButton_1_1_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			btnNewButton_1_1_1.setIcon(new ImageIcon("C:\\Users\\User-02\\Documents\\Projets\\Sparadrap\\src\\images\\bouton-quitter-vers-lapplication.png"));
			btnNewButton_1_1_1.setBounds(new Rectangle(0, 0, 50, 50));
		}
		return btnNewButton_1_1_1;
	}
}
