package vue;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.MatteBorder;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;

public class AchatOrdonnance extends JFrame {

	private JPanel contentPane;
	private JLabel lblNewLabel;
	private JLabel lblNewLabel_1;
	private JButton btnNewButton;
	private JButton btnOrdonance;
	private JPanel panel;
	private JTextField textField_1;
	private JTextField textField_2;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_2_1;
	private JLabel lblNewLabel_2_2;
	private JLabel lblNewLabel_2_3;
	private JLabel lblNewLabel_2_5;
	private JTextField textField_4;
	private JTextField textField_5;
	private JLabel lblNewLabel_2_5_1;
	private JLabel lblNewLabel_2_5_2;
	private JTextField textField_6;
	private JButton btnNewButton_1;
	private JComboBox comboBox;
	private JLabel lblNewLabel_2_3_1;
	private JTextField textField;
	private JLabel lblNewLabel_2_4;
	private JButton btnNewButton_1_1;
	private JButton btnNewButton_1_1_1;
	private JLabel lblNewLabel_3;
	private JLabel lblNewLabel_3_1;
	private JButton btnPay;
	private JButton btnValider;
	private JLabel lblNewLabel_2_2_1;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AchatOrdonnance frame = new AchatOrdonnance();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AchatOrdonnance() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 650);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(getBtnNewButton_1_1(), GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addGap(180)
					.addComponent(getLblNewLabel(), GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 186, Short.MAX_VALUE)
					.addComponent(getBtnNewButton_1_1_1(), GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
					.addGap(36))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(112)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(getLblNewLabel_2_5_2(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(getTextField_6(), GroupLayout.PREFERRED_SIZE, 176, GroupLayout.PREFERRED_SIZE)
							.addGap(299))
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(getLblNewLabel_2_5(), GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(getTextField_4(), GroupLayout.PREFERRED_SIZE, 176, GroupLayout.PREFERRED_SIZE)
								.addGap(299))
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(getBtnNewButton(), GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
								.addGap(97)
								.addComponent(getLblNewLabel_1(), GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED, 105, Short.MAX_VALUE)
								.addComponent(getBtnOrdonance(), GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
								.addGap(111)))))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(176)
					.addComponent(getLblNewLabel_2_4(), GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
					.addGap(89)
					.addComponent(getLblNewLabel_3(), GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(getBtnPay(), GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(85, Short.MAX_VALUE))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(55)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 556, GroupLayout.PREFERRED_SIZE)
						.addComponent(getBtnNewButton_1(), GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(75, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(28)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
							.addComponent(getBtnNewButton_1_1(), GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
							.addGroup(gl_contentPane.createSequentialGroup()
								.addComponent(getLblNewLabel(), GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
								.addGap(18)
								.addComponent(getLblNewLabel_1(), GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
							.addComponent(getBtnNewButton_1_1_1(), GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
						.addComponent(getBtnNewButton())
						.addComponent(getBtnOrdonance()))
					.addGap(30)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblNewLabel_2_5(), GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
						.addComponent(getTextField_4(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblNewLabel_2_5_2(), GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
						.addComponent(getTextField_6(), GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(15)
					.addComponent(getPanel(), GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(getBtnNewButton_1())
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(getLblNewLabel_2_4(), GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addComponent(getLblNewLabel_3(), GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addComponent(getBtnPay()))
					.addContainerGap(59, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}

	private JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("Sparadrap");
			lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 26));
		}
		return lblNewLabel;
	}
	private JLabel getLblNewLabel_1() {
		if (lblNewLabel_1 == null) {
			lblNewLabel_1 = new JLabel("");
			lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\User-02\\Documents\\Projets\\Sparadrap\\src\\images\\My project-1.png"));
		}
		return lblNewLabel_1;
	}
	private JButton getBtnNewButton() {
		if (btnNewButton == null) {
			btnNewButton = new JButton("Direct");
			btnNewButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new AchatDirect();
					AchatDirect achatDirect = new AchatDirect();
					achatDirect.setVisible(true);
					achatDirect.setTitle("Achat Direct");
					dispose();
				}
			});
		}
		return btnNewButton;
	}
	private JButton getBtnOrdonance() {
		if (btnOrdonance == null) {
			btnOrdonance = new JButton("Ordonnance");
			btnOrdonance.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new AchatOrdonnance();
					AchatOrdonnance achatOrdonnance = new AchatOrdonnance();
					achatOrdonnance.setVisible(true);
					achatOrdonnance.setTitle("Achat Ordonnance");
					dispose();
				}
			});
		}
		return btnOrdonance;
	}
	private JPanel getPanel() {
		if (panel == null) {
			panel = new JPanel();
			panel.setLayout(null);
			panel.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
			panel.add(getTextField_1());
			panel.add(getTextField_2());
			panel.add(getLblNewLabel_2());
			panel.add(getLblNewLabel_2_1());
			panel.add(getLblNewLabel_2_2());
			panel.add(getLblNewLabel_2_3());
			panel.add(getTextField_5());
			panel.add(getLblNewLabel_2_5_1());
			panel.add(getComboBox());
			panel.add(getLblNewLabel_2_3_1());
			panel.add(getTextField());
			panel.add(getLblNewLabel_3_1());
			panel.add(getBtnValider());
			panel.add(getLblNewLabel_2_2_1());
			panel.add(getTextField_3());
		}
		return panel;
	}
	private JTextField getTextField_1() {
		if (textField_1 == null) {
			textField_1 = new JTextField();
			textField_1.setColumns(10);
			textField_1.setBounds(274, 102, 176, 20);
		}
		return textField_1;
	}
	private JTextField getTextField_2() {
		if (textField_2 == null) {
			textField_2 = new JTextField();
			textField_2.setColumns(10);
			textField_2.setBounds(274, 133, 176, 20);
		}
		return textField_2;
	}
	private JLabel getLblNewLabel_2() {
		if (lblNewLabel_2 == null) {
			lblNewLabel_2 = new JLabel("Médicament");
			lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNewLabel_2.setBounds(115, 60, 95, 14);
		}
		return lblNewLabel_2;
	}
	private JLabel getLblNewLabel_2_1() {
		if (lblNewLabel_2_1 == null) {
			lblNewLabel_2_1 = new JLabel("Prix unitaire");
			lblNewLabel_2_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNewLabel_2_1.setBounds(115, 103, 85, 14);
		}
		return lblNewLabel_2_1;
	}
	private JLabel getLblNewLabel_2_2() {
		if (lblNewLabel_2_2 == null) {
			lblNewLabel_2_2 = new JLabel("Quantité");
			lblNewLabel_2_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNewLabel_2_2.setBounds(115, 134, 67, 14);
		}
		return lblNewLabel_2_2;
	}
	private JLabel getLblNewLabel_2_3() {
		if (lblNewLabel_2_3 == null) {
			lblNewLabel_2_3 = new JLabel("Stock");
			lblNewLabel_2_3.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNewLabel_2_3.setBounds(125, 226, 46, 14);
		}
		return lblNewLabel_2_3;
	}
	private JLabel getLblNewLabel_2_5() {
		if (lblNewLabel_2_5 == null) {
			lblNewLabel_2_5 = new JLabel("Nom");
			lblNewLabel_2_5.setFont(new Font("Tahoma", Font.PLAIN, 16));
		}
		return lblNewLabel_2_5;
	}
	private JTextField getTextField_4() {
		if (textField_4 == null) {
			textField_4 = new JTextField();
			textField_4.setColumns(10);
		}
		return textField_4;
	}
	private JTextField getTextField_5() {
		if (textField_5 == null) {
			textField_5 = new JTextField();
			textField_5.setColumns(10);
			textField_5.setBounds(274, 26, 176, 20);
		}
		return textField_5;
	}
	private JLabel getLblNewLabel_2_5_1() {
		if (lblNewLabel_2_5_1 == null) {
			lblNewLabel_2_5_1 = new JLabel("Date");
			lblNewLabel_2_5_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNewLabel_2_5_1.setBounds(115, 27, 95, 14);
		}
		return lblNewLabel_2_5_1;
	}
	private JLabel getLblNewLabel_2_5_2() {
		if (lblNewLabel_2_5_2 == null) {
			lblNewLabel_2_5_2 = new JLabel("Prénom");
			lblNewLabel_2_5_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		}
		return lblNewLabel_2_5_2;
	}
	private JTextField getTextField_6() {
		if (textField_6 == null) {
			textField_6 = new JTextField();
			textField_6.setColumns(10);
		}
		return textField_6;
	}
	private JButton getBtnNewButton_1() {
		if (btnNewButton_1 == null) {
			btnNewButton_1 = new JButton("Ajouter");
		}
		return btnNewButton_1;
	}
	private JComboBox getComboBox() {
		if (comboBox == null) {
			comboBox = new JComboBox();
			comboBox.setModel(new DefaultComboBoxModel(new String[] {"Doliprane", "Efferalgan", "Dafalgan"}));
			comboBox.setBounds(274, 58, 176, 22);
		}
		return comboBox;
	}
	private JLabel getLblNewLabel_2_3_1() {
		if (lblNewLabel_2_3_1 == null) {
			lblNewLabel_2_3_1 = new JLabel("Prise en charge");
			lblNewLabel_2_3_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNewLabel_2_3_1.setBounds(125, 263, 118, 14);
		}
		return lblNewLabel_2_3_1;
	}
	private JTextField getTextField() {
		if (textField == null) {
			textField = new JTextField();
			textField.setColumns(10);
			textField.setBounds(274, 262, 176, 20);
		}
		return textField;
	}
	private JLabel getLblNewLabel_2_4() {
		if (lblNewLabel_2_4 == null) {
			lblNewLabel_2_4 = new JLabel("Prix Total");
			lblNewLabel_2_4.setFont(new Font("Tahoma", Font.PLAIN, 16));
		}
		return lblNewLabel_2_4;
	}
	private JButton getBtnNewButton_1_1() {
		if (btnNewButton_1_1 == null) {
			btnNewButton_1_1 = new JButton("");
			btnNewButton_1_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					new AchatDirect();
					AchatDirect achatDirect = new AchatDirect();
					achatDirect.setVisible(true);
					dispose();
				}
			});
			btnNewButton_1_1.setIcon(new ImageIcon("C:\\Users\\User-02\\Documents\\Projets\\Sparadrap\\src\\images\\en-arriere.png"));
			btnNewButton_1_1.setBounds(new Rectangle(0, 0, 50, 50));
		}
		return btnNewButton_1_1;
	}
	private JButton getBtnNewButton_1_1_1() {
		if (btnNewButton_1_1_1 == null) {
			btnNewButton_1_1_1 = new JButton("");
			btnNewButton_1_1_1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
			btnNewButton_1_1_1.setIcon(new ImageIcon("C:\\Users\\User-02\\Documents\\Projets\\Sparadrap\\src\\images\\bouton-quitter-vers-lapplication.png"));
			btnNewButton_1_1_1.setBounds(new Rectangle(0, 0, 50, 50));
		}
		return btnNewButton_1_1_1;
	}
	private JLabel getLblNewLabel_3() {
		if (lblNewLabel_3 == null) {
			lblNewLabel_3 = new JLabel("Prix");
		}
		return lblNewLabel_3;
	}
	private JLabel getLblNewLabel_3_1() {
		if (lblNewLabel_3_1 == null) {
			lblNewLabel_3_1 = new JLabel("Prix");
			lblNewLabel_3_1.setBounds(274, 228, 171, 19);
		}
		return lblNewLabel_3_1;
	}
	private JButton getBtnPay() {
		if (btnPay == null) {
			btnPay = new JButton("Payé");
		}
		return btnPay;
	}
	private JButton getBtnValider() {
		if (btnValider == null) {
			btnValider = new JButton("Valider");
			btnValider.setBounds(467, 165, 79, 23);
		}
		return btnValider;
	}
	private JLabel getLblNewLabel_2_2_1() {
		if (lblNewLabel_2_2_1 == null) {
			lblNewLabel_2_2_1 = new JLabel("Mutuelle");
			lblNewLabel_2_2_1.setFont(new Font("Tahoma", Font.PLAIN, 16));
			lblNewLabel_2_2_1.setBounds(115, 174, 67, 14);
		}
		return lblNewLabel_2_2_1;
	}
	private JTextField getTextField_3() {
		if (textField_3 == null) {
			textField_3 = new JTextField();
			textField_3.setColumns(10);
			textField_3.setBounds(274, 166, 176, 20);
		}
		return textField_3;
	}
}
