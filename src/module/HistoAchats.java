package module;

import java.time.LocalDate;
import java.util.Date;

import Exceptions.MissMatchException;

public class HistoAchats /*extends Medicament */ {

	private LocalDate DateAchat;
	public boolean TypeAchat;
	public String NomMedicament;
	public Double Prix;
	public String NomClient;
	public String PrenomClient;
	public int Quantite;
	
	
	
	

	
	
	public HistoAchats(String pNomMedicament,Double pPrix,String pNomClient,String pPrenomClient, int pQuantite,boolean pTypeAchat) {
	this.setDateAchat();
	this.setNomMedicament(pNomMedicament);
	this.setPrix(pPrix);
	this.setNomClient(pNomClient);
	this.setPrenomClient(pPrenomClient);
	this.setQuantite(pQuantite);
	this.setTypeAchat(pTypeAchat);
	}
	public HistoAchats(LocalDate pDate,String pNomMedicament,Double pPrix,String pNomClient,String pPrenomClient, int pQuantite,boolean pTypeAchat) {
		this.setDateAchat(pDate);
		this.setNomMedicament(pNomMedicament);
		this.setPrix(pPrix);
		this.setNomClient(pNomClient);
		this.setPrenomClient(pPrenomClient);
		this.setQuantite(pQuantite);
		this.setTypeAchat(pTypeAchat);
	}
	
	/**
     * Constructeur de classe
     * 
     * @param DateAchat
     * @param TypeAchat
     * @param nommedicament
     * @param categorie
     * @param prix
     * @param nomClient
     * @param Quantite
     */ 

	public LocalDate getDateAchat() {
		return DateAchat;
	}

	public void setDateAchat(LocalDate pDate) {
		DateAchat = pDate;
	}
	public void setDateAchat() {
		DateAchat = LocalDate.now();
	}

	public boolean isTypeAchat() {
		return TypeAchat;
	}

	public void setTypeAchat(boolean typeAchat) {
		TypeAchat = typeAchat;
	}


	public String getNomMedicament() {
		return NomMedicament;
	}


	public void setNomMedicament(String nomMedicament) {
		NomMedicament = nomMedicament;
	}


	public Double getPrix() {
		return Prix;
	}


	public void setPrix(Double prix) {
		Prix = prix;
	}


	public String getNomClient() {
		return NomClient;
	}


	public void setNomClient(String nomClient) {
		NomClient = nomClient;
	}


	public String getPrenomClient() {
		return PrenomClient;
	}


	public void setPrenomClient(String prenomClient) {
		PrenomClient = prenomClient;
	}


	public int getQuantite() {
		return Quantite;
	}


	public void setQuantite(int quantite) {
		Quantite = quantite;
	}

	
}
