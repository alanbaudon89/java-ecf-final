package module;

import java.util.Arrays;

import Exceptions.MissMatchException;


public class Medicament {

	public String SelectedItem = null;
	public String NomMedicament;
	private String Categorie;
	private Double Prix;
	private String DateMiseEnService;
	private int Quantite;


	public Medicament(String pNomMedicament, String pCategorie,
			Double pPrix, String DateMiseEnService, int pQuantite) {
		this.setNomMedicament(pNomMedicament);
		this.setCategorie(pCategorie);
		this.setPrix(pPrix);
		this.setDateMiseEnService(DateMiseEnService);
		this.setQuantite(pQuantite);
	}

		
		/*Getters - Setters */
	/**
     * Constructeur de classe
     * 
     * @param nommedicament
     * @param categorie
     * @param prix
     * @param dateMiseEnService
     * @param Quantite
     */ 
	public  String getNomMedicament() {
		return NomMedicament;
	}

	public void setNomMedicament(String nomMedicament) {
		NomMedicament = nomMedicament;
	}

	public String getCategorie() {
		return Categorie;
	}

	public void setCategorie(String categorie) {
		Categorie = categorie;
	}

	public Double getPrix() {
		return Prix;
	}

	public void setPrix(Double pPrix) {
		Prix = pPrix;
	}

	public String getDateMiseEnService() {
		return DateMiseEnService;
	}

	public void setDateMiseEnService(String dateMiseEnService) {
		DateMiseEnService = dateMiseEnService;
	}

	public int getQuantite() {
		return Quantite;
	}

	public void setQuantite(int quantite) {
		Quantite = quantite;
	}
	
	public String toString()
	{
		return this.getNomMedicament();
	}
	
	
}
