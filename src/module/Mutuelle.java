package module;

import Exceptions.MissMatchException;

public class Mutuelle {

	
	private String Nom;
	private String Ville;
	private String Rue;
	private int CodePostal;
	private String Telephone;
	private String Email;
	private String Departement;
	private String TauxPriseEnCharge;
	
	/**
     * Constructeur de classe
     * 
     * @param nom
     * @param prenom
     * @param codePostal
     * @param ville
     * @param rue
     * @param telephone
     * @param email
     * @param departement
     * @param TauxPriseEnCharge
     * @throws MissMatchException
     */ 
	
	public String getNom() {
		return Nom;
	}
		
	public void setNom(String nom) {
		Nom = nom;
	}
	public String getVille() {
		return Ville;
	}
	public void setVille(String ville) {
		Ville = ville;
	}
	public String getRue() {
		return Rue;
	}
	public void setRue(String rue) {
		Rue = rue;
	}
	public int getCodePostal() {
		return CodePostal;
	}
	public void setCodePostal(int codePostal) {
		CodePostal = codePostal;
	}
	public String getTelephone() {
		return Telephone;
	}
	public void setTelephone(String telephone) {
		Telephone = telephone;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
	public String getDepartement() {
		return Departement;
	}
	public void setDepartement(String departement) {
		Departement = departement;
	}
	public String getTauxPriseEnCharge() {
		return TauxPriseEnCharge;
	}
	public void setTauxPriseEnCharge(String tauxPriseEnCharge) {
		TauxPriseEnCharge = tauxPriseEnCharge;
	}
}
