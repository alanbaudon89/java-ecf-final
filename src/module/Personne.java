package module;

/**
* Declarations des imports
*/
import java.util.regex.Pattern;

import Exceptions.MissMatchException;

public class Personne {

	
	private String Nom;
	private String Prenom;
	private String Ville;
	private String CodePostal;
	private String Rue;
	private String Telephone;
	private String Email;
	public String getNom() {
		return Nom;
			
	}
	
	public Personne(String nom, String prenom, String Rue, String codePostal, String ville, String telephone,
            String email) throws MissMatchException {
			this.setNom(nom);
            this.setPrenom(prenom);
            this.setRue(Rue);
            this.setCodePostal(codePostal);
            this.setVille(ville);
            this.setTelephone(telephone);
            this.setEmail(email);};
	
	/*Getter - Setters*/
	
	/*public void setNom(String nom) {
		Nom = nom;
	}*/
	
	/**
     * Constructeur de classe
     * 
     * @param nom
     * @param prenom
     * @param codePostal
     * @param ville
     * @param rue
     * @param telephone
     * @param email
     * @throws MissMatchException
     */ 
	
	public void setNom(String nom) throws MissMatchException{
        if (nom==null||nom.trim().isEmpty()) {
            throw new MissMatchException("Saisie du nom incorrecte");}
        if(!Pattern.matches("([a-zA-Z]+[-éèçàîêôë]*)+", nom)) {
            throw new MissMatchException("Veuillez entrer un nom valide ");
        }
        /*else */{
        this.Nom = nom;}
    }
	public String getPrenom() {
		return Prenom;
	}
	public void setPrenom(String prenom) {
		Prenom = prenom;
	}
	public String getVille() {
		return Ville;
	}
	public void setVille(String ville) {
		Ville = ville;
	}
	public String getCodePostal() {
		return CodePostal;
	}
	public void setCodePostal(String codePostal) {
		CodePostal = codePostal;
	}
	public String getRue() {
		return Rue;
	}
	public void setRue(String rue) {
		Rue = rue;
	}
	public String getTelephone() {
		return Telephone;
	}
	public void setTelephone(String telephone) {
		Telephone = telephone;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
}
