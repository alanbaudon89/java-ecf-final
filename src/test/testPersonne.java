package test;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import Exceptions.MissMatchException;
import module.Personne;

public class testPersonne {
	
	private static Personne pers;
	
	@BeforeAll
    static void initialize() {
        try {
             pers = new Personne("Dupond", "Jeanne", "8 rue de loin", "54570", "Foug", "0601020305",
                    "MarieJeanne420@Wanadoo.fr");
        } catch (MissMatchException mme) {
            mme.printStackTrace();
        }
    }

    @Test
    @DisplayName("Constructeur correct")
    void testPersonneIntStringStringStringStringStringIntIntInt() {
        assertTrue(pers.getNom() == "Dupond");
        assertTrue(pers.getPrenom() == "Jeanne");
        assertTrue(pers.getRue() == "8 rue de loin");
        assertTrue(pers.getCodePostal() == "54570");
        assertTrue(pers.getVille() == "Foug");
        assertTrue(pers.getTelephone() == "0601020305");
        assertTrue(pers.getEmail() == "MarieJeanne420@Wanadoo.fr");

    }
    @Test
    @DisplayName("Nom de la personne avec champ null")
    void testSetNomPersonneNull() {
        try {
            pers.setNom(null);
        } catch (MissMatchException mme) {
            assert (mme.getMessage().equals("Saisie du nom incorrecte"));
        }
    }

    @Test
    @DisplayName("Nom de la personne avec champ vide")
    void testSetNomPersonneEmpty() {
        try {
            pers.setNom("");
        } catch (MissMatchException mme) {
            assert (mme.getMessage().equals("Saisie du nom incorrecte"));
        }
    }

    @Test
    @DisplayName("Nom de la personne faussement ecrit")
    void testSetNomPersonneFalse() {
        try {
            pers.setNom("858");
        } catch (MissMatchException mme) {
            assert (mme.getMessage().equals("Veuillez entrer un nom valide "));
        }
    }
}
/*private String Nom;
private String Prenom;
private String Ville;
private String CodePostal;
private String Rue;
private String Telephone;
private String Email;*/